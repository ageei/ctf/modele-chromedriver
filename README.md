# Docker image for XSS challenges using chromedriver

This image provides a Docker image on which to build a
XSS challenge. It builds on php:7.0-apache and installs
Google Chrome and the chromedriver. The browser can be
invoked directly or you may invoke the driver using selenium
or puppeteer.

## Usage

Build your own Docker image with this example Dockerfil:

```
FROM registry.gitlab.com/ageei/ctf/modele-chromedriver

COPY htdocs /var/www/html
```

An example PHP script is provided in `htdocs/`.

## Keeping the image up to date

Check the following page for new versions of the chromedriver:
https://sites.google.com/a/chromium.org/chromedriver/downloads
and update the version number in `tools/install.sh`.

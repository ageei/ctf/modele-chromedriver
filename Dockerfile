FROM php:7.0-apache


COPY	tools/install.sh /install.sh

RUN	apt-get update && \
	apt-get -y upgrade && \
	apt-get -y install \
		curl \
		unzip \
	&& bash /install.sh && \
	rm -f /install.sh && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/*

#!/bin/sh
#
# Based on the following Heroku buildpacks:
#
# https://github.com/heroku/heroku-buildpack-google-chrome
# https://github.com/heroku/heroku-buildpack-chromedriver
#
set -eu

channel="${1:-stable}"
driver="${2:-73.0.3683.20}"

case "${channel}" in
	stable|beta|unstable)
		;;
	*)
		printf 'Only the stable, beta and unstable channels are available\n' >&2
		;;
esac


printf '[I] Installing google-chrome\n'

curl -Ls -o gchrome.deb \
	"https://dl.google.com/linux/direct/google-chrome-${channel}_current_amd64.deb"

dpkg -i gchrome.deb || :
apt-get -y install -f
rm -f gchrome.deb


printf '[I] Creating google-chrome headless wrapper\n'

mv /usr/bin/google-chrome-${channel} /usr/bin/.google-chrome-${channel}
cat >/usr/bin/google-chrome-${channel} << __EOF__
#!/bin/sh
##

tmpdir=\$(mktemp -d)
cleanup() {
	rm -rf "\${tmpdir}"
}
trap cleanup EXIT INT QUIT TERM

export HOME="\${tmpdir}"
exec /usr/bin/.google-chrome-${channel} \
	--disable-gpu \
	--no-sandbox \
	--headless \
	"\${@}"
__EOF__
chmod +x "/usr/bin/google-chrome-${channel}"


printf '[I] Installing chromedriver\n'

curl -Ls -o zip "https://chromedriver.storage.googleapis.com/${driver}/chromedriver_linux64.zip"

unzip zip
mv chromedriver /usr/bin
rm -f zip
